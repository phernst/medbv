#include "dcmtk/config/osconfig.h"
#include "dcmtk/dcmimgle/dcmimage.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <fstream>
#include <cmath>

#include "featureVec.h"
#include "Candidate.hpp"

// uncomment for faster output
//#define SMALL_REGION

typedef std::vector<cv::Point> Contour;
static Contour ellipseToContour(const cv::RotatedRect& rr, const cv::Size2f& imgsize = {512, 512})
{
  cv::Mat matDraw = cv::Mat::zeros(imgsize, CV_8UC1);
  cv::ellipse(matDraw, rr, {255});

  std::vector<Contour> cts;
  cv::findContours(matDraw, cts, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

  return cts.at(0);
}

static Contour::size_type nearestPointContour(const Contour& cnt, const cv::Point& p)
{
  assert(cnt.size() != 0);
  float minDist = (p.x - cnt[0].x)*(p.x - cnt[0].x) + (p.y - cnt[0].y)*(p.y - cnt[0].y);
  Contour::size_type pos = 0;
  for (Contour::size_type i = 1; i < cnt.size(); ++i)
  {
    float dist = (p.x-cnt[i].x)*(p.x-cnt[i].x) + (p.y-cnt[i].y)*(p.y-cnt[i].y);
    if (dist < minDist)
    {
      minDist = dist;
      pos = i;
    }
  }
  return pos;
}

static cv::Vec2f rrToVec(const cv::RotatedRect& rr)
{
  cv::Point2f pts[4];
  rr.points(pts);
  cv::Vec2f l1 = {pts[0].x - pts[1].x, pts[0].y - pts[1].y};
  cv::Vec2f l2 = {pts[0].x - pts[3].x, pts[0].y - pts[3].y};
  if (l1.dot(l1) < l2.dot(l2))
  {
    return l2;
  }
  return l1;
}

static cv::Vec2f tangentVectorAtContourPoint(const Contour& cnt, Contour::size_type pos)
{
  cv::Point nextPoint = cnt.at((pos + 1) % cnt.size());
  cv::Point prevPoint = cnt.at((pos - 1) % cnt.size());
  cv::Vec2f diffPoint =
    { static_cast<float>(nextPoint.x - prevPoint.x),
      static_cast<float>(nextPoint.y - prevPoint.y)};
  return diffPoint;
}

static float compareVectors(const cv::Vec2f& v1, const cv::Vec2f& v2)
{
  float l1 = cv::norm(v1);
  float l2 = cv::norm(v2);

  float dot1 = v1.dot(v2)/(l1*l2);
  float dot2 = v1.dot(-v2)/(l1*l2);

  return dot1 > dot2 ? dot1 : dot2;
}

static cv::Mat rrToMask(const cv::RotatedRect& rr, const cv::Size& s)
{
  cv::Mat result = cv::Mat::zeros(s.height, s.width, CV_8UC1);
  cv::Point2f pts2f[4];
  cv::Point pts[4];
  rr.points(pts2f);
  for (unsigned i = 0; i < 4; ++i)
  {
    pts[i] = pts2f[i];
  }
  cv::fillConvexPoly(result, &pts[0], 4, cv::Scalar(255));
  return result;
}

static float getHomogeneity(const cv::Mat& img, const cv::RotatedRect& rr)
{
  cv::Mat mask = rrToMask(rr, {img.cols, img.rows});
  cv::Mat maskImg = img.clone();
  maskImg.setTo(0, mask == 0);

  // homogeneity: mean value
  return cv::sum(maskImg)[0] / cv::sum(mask)[0];
}

static cv::Mat threshold16(const cv::Mat& src, uint16_t thresh)
{
  cv::Mat result = src.clone();
  uint16_t* myData = reinterpret_cast<uint16_t*>(result.data);
  for (unsigned i = 0; i < result.rows * result.cols; ++i)
  {
    myData[i] = myData[i] < thresh ? 0 : myData[i];
  }
  return result;
}

static cv::RotatedRect calculateBestRect(const cv::Mat& matImg, const cv::RotatedRect& inputRect)
{
  float fWidth = inputRect.size.width < inputRect.size.height
    ? inputRect.size.width
    : inputRect.size.height;
  float fHeight = inputRect.size.height < inputRect.size.width
    ? inputRect.size.width
    : inputRect.size.height;
  float fAngle = inputRect.size.width < inputRect.size.height
    ? inputRect.angle
    : inputRect.angle + 90.0f;
  cv::RotatedRect rrAligned(inputRect.center, {fWidth, fHeight}, fAngle);

  // different angles
  float fMaxGray = 0.0f;
  float fMaxAngle = 0.0f;
  float diag = std::sqrt(
    rrAligned.size.height*rrAligned.size.height
    + rrAligned.size.width*rrAligned.size.width);
  float alpha = std::asin(rrAligned.size.width/diag)*180.f/3.14159f;
#ifdef SMALL_REGION
  for (int i = -1; i <= 1; ++i)
#else
  for (int i = -5; i <= 5; ++i)
#endif
  {
#ifdef SMALL_REGION
    for (int j = -1; j <= 1; ++j)
#else
    for (int j = -5; j <= 5; ++j)
#endif
    {
      for (float f = fAngle - alpha; f <= fAngle + alpha; f += 1.f)
      {
	cv::RotatedRect rrTemp = rrAligned;
	rrTemp.size.width = static_cast<float>(3);
	rrTemp.angle = f;
	rrTemp.center = {rrTemp.center.x + i, rrTemp.center.y + j};

	cv::Point2f vertices2f[4];
	cv::Point vertices[4];
	rrTemp.points(vertices2f);
	for (unsigned i = 0; i < 4; ++i)
	{
	  vertices[i] = vertices2f[i];
	}

	cv::Mat matBinMask = cv::Mat::zeros(matImg.rows, matImg.cols, CV_8UC1);
	cv::fillConvexPoly(matBinMask, vertices, 4, cv::Scalar(1));
	cv::Mat matImgMask = cv::Mat::zeros(matImg.rows, matImg.cols, CV_8UC1);
	cv::multiply(matBinMask, matImg, matImgMask);
	float fMeanGray = cv::sum(matImgMask)[0] / cv::sum(matBinMask)[0];

	
	if (fMeanGray > fMaxGray)
	{
	  fMaxGray = fMeanGray;
	  fMaxAngle = f;
	}

      }
    }
  }
  cv::RotatedRect rrResult(
    rrAligned.center,
    {1, rrAligned.size.height},
    fMaxAngle);

  return rrResult;
}

static bool similarAngle(float angle1, float angle2)
{
  return std::abs(angle1 - angle2) < 20.f;
}

static bool isInStrip(const cv::Point2f& stripPoint, float width, float angle, const cv::Point2f& testPoint)
{
  cv::Mat mask = rrToMask({stripPoint, {width, 1024.f}, angle}, {512, 512});

  return mask.at<unsigned char>(testPoint.y, testPoint.x) != 0;
}

static cv::Point2f getLowestYPoint(const cv::RotatedRect& rr)
{
  cv::Point2f pts[4];
  rr.points(pts);
  unsigned pos = 0;
  float minY = pts[0].y;
  for (unsigned i = 1; i < 4; ++i)
  {
    if (pts[i].y < minY)
    {
      minY = pts[i].y;
      pos = i;
    }
  }
  return pts[pos];
}

static cv::Point2f getHighestYPoint(const cv::RotatedRect& rr)
{
  cv::Point2f pts[4];
  rr.points(pts);
  unsigned pos = 0;
  float maxY = pts[0].y;
  for (unsigned i = 1; i < 4; ++i)
  {
    if (pts[i].y > maxY)
    {
      maxY = pts[i].y;
      pos = i;
    }
  }
  return pts[pos];  
}

static std::vector<Candidate> connectCandidates(const std::vector<Candidate>& cdts)
{
  std::vector<Candidate> result;
  std::vector<unsigned> vecIndices;
  for (unsigned i = 0; i < cdts.size(); ++i)
  {
    vecIndices.push_back(i);
  }
  while(!vecIndices.empty())
  {
    const auto& currIndexIt = vecIndices.begin();
    auto currAngle = cdts[*currIndexIt].getRotatedRectangle().angle;
    std::vector<unsigned> vecToBeConnected;
    vecToBeConnected.push_back(*currIndexIt);
    
    // find rects with similar angle
    std::vector<unsigned> vecSimilarAngle;
    for (auto it = vecIndices.begin() + 1; it != vecIndices.end(); ++it)
    {
      if (similarAngle(currAngle, cdts[*it].getRotatedRectangle().angle))
      {
	vecSimilarAngle.push_back(*it);
      }
    }

    // find rects that are probably on the line
    for (auto it = vecSimilarAngle.begin(); it != vecSimilarAngle.end(); ++it)
    {
      if (isInStrip(cdts[*currIndexIt].getRotatedRectangle().center, 20.f, currAngle, cdts[*it].getRotatedRectangle().center))
      {
	vecToBeConnected.push_back(*it);
      }
    }

    if (vecToBeConnected.size() == 1)
    {
      result.push_back(cdts[vecToBeConnected[0]]);
      vecIndices.erase(std::find(vecIndices.begin(), vecIndices.end(), vecToBeConnected[0]));
      continue;
    }

    // connect lines
    // sort by y of center point
    std::sort(vecToBeConnected.begin(), vecToBeConnected.end(), [&cdts](unsigned l, unsigned r)
	      {
		return cdts[l].getRotatedRectangle().center.y
		  < cdts[r].getRotatedRectangle().center.y;
	      });

    // add connected line to result
    cv::Point2f lowestYPoint = cdts[vecToBeConnected[0]].getRotatedRectangle().center;
    cv::Point2f highestYPoint = cdts[vecToBeConnected[vecToBeConnected.size() - 1]].getRotatedRectangle().center;
    cv::Point2f pointDiff = (lowestYPoint - highestYPoint)/cv::norm(lowestYPoint - highestYPoint);
    lowestYPoint += pointDiff*1024.f;
    highestYPoint -= pointDiff*cdts[vecToBeConnected[vecToBeConnected.size() - 1]].getRotatedRectangle().size.height*0.5f;
    cv::Point2f center = 0.5f*(lowestYPoint + highestYPoint);
    float veclength = cv::norm(lowestYPoint - highestYPoint);
    float angle = std::asin((lowestYPoint.x - highestYPoint.x)/veclength)*180.f/3.14159f;
    result.push_back(Candidate({center, {1, veclength}, angle}));

    // remove processed indices
    for (unsigned i : vecToBeConnected)
    {
      vecIndices.erase(std::find(vecIndices.begin(), vecIndices.end(), i));
    }
  }
  return result;
}

static cv::Point2f findNeedleTip(const cv::Mat& img, const cv::RotatedRect& rr)
{
  cv::Mat imggray;
  cv::cvtColor(img, imggray, cv::COLOR_BGR2GRAY);
  cv::Mat gradX, gradY;
  cv::Sobel(imggray, gradX, CV_16S, 1, 0, 3);
  cv::Sobel(imggray, gradY, CV_16S, 0, 1, 3);
  gradX = cv::abs(gradX);
  gradY = cv::abs(gradY);

  cv::Mat gradImage = 0.5*gradX + 0.5*gradY;
  gradImage.convertTo(gradImage, CV_8UC1);

  cv::RotatedRect rrCompleteLine(rr.center, {1, 5.f*img.size().width}, rr.angle);
  cv::Mat maskCompleteLine = rrToMask(rrCompleteLine, img.size());
  maskCompleteLine.setTo(1, maskCompleteLine != 0);

  cv::Mat maskGradImage;
  cv::multiply(maskCompleteLine, gradImage, maskGradImage);

  typedef uint8_t gradimg_t;
  cv::Point pMax = {0,0};
  gradimg_t maxValue = 0;
  int miny = std::max(static_cast<int>(getHighestYPoint(rr).y), 0);
  for (int i = miny; i < maskGradImage.rows; ++i)
  {
    for (int j = 0; j < maskGradImage.cols; ++j)
    {
      if ((i-256)*(i-256)+(j-256)*(j-256) >= 250*250)
      {
      	continue;
      }
      if (std::abs(maskGradImage.at<gradimg_t>(i, j)) >= maxValue)
      {
	maxValue = std::abs(maskGradImage.at<gradimg_t>(i, j));
	pMax = {j,i};
      }
    }
  }
  return pMax;
}

// contains a sample of the ground truth
struct GroundTruthSample
{
  std::string filename;
  float tipX, tipY;
  float endX, endY;

  GroundTruthSample(const std::string& filename, float tipX, float tipY, float endX, float endY)
    : filename(filename), tipX(tipX), tipY(tipY), endX(endX), endY(endY) {}
};

static std::vector<GroundTruthSample> readCSV(const std::string& filename)
{
  std::vector<GroundTruthSample> result;
  std::ifstream ifFile(filename);

  if (!ifFile.bad())
  {
    std::string line;
    while(std::getline(ifFile, line))
    {
      std::stringstream ss(line);
      std::string strFilename, strTipX, strTipY, strEndX, strEndY;
      std::getline(ss, strFilename, ';');
      std::getline(ss, strTipX, ';');
      std::getline(ss, strTipY, ';');
      std::getline(ss, strEndX, ';');
      std::getline(ss, strEndY, ';');
      result.push_back(GroundTruthSample(
			 strFilename,
			 strTipX == "NaN" ? NAN : std::stof(strTipX),
			 strTipY == "NaN" ? NAN : std::stof(strTipY),
			 strEndX == "NaN" ? NAN : std::stof(strEndX),
			 strEndY == "NaN" ? NAN : std::stof(strEndY)
			 ));
    }
  }
  
  return result;
}

int main(int argn, char* args[])
{
  std::locale::global(std::locale(""));

//  auto csvValues = readCSV("p2_needle_positions.csv");
  
  std::vector<std::string> v;
  for (int i = 1; i < argn; ++i)
  {
    v.push_back(args[i]);
  }
  
  cv::VideoWriter vw(
    "output.avi",
    cv::VideoWriter::fourcc('D', 'I', 'V', 'X'),
    4.0,
    cv::Size(512, 512),
    true);
  
//  std::ofstream outFile("result.csv");
//  outFile << "truthTip;truthAngle;needleTip;needleAngle;errorTip;errorAngle;errorSum\n";
  for (unsigned iFilePos = 0; iFilePos < v.size(); ++iFilePos)
  {
    const std::string& str = v.at(iFilePos);
    std::cout << "Processing " << str << " ... ";
    std::unique_ptr<DicomImage> image(new DicomImage(str.c_str()));
    
    unsigned long ulSize = image->getOutputDataSize();
    std::unique_ptr<uint16_t[]> a_uiImage(new uint16_t[ulSize]);
    image->getOutputData(a_uiImage.get(), ulSize);
    
    cv::Mat matImg(512, 512, CV_16UC1, a_uiImage.get());
    cv::Mat matOutput = matImg.clone();

    cv::medianBlur(matImg, matImg, 3);

    matImg.setTo((1 << 15) + 200, matImg < ((1 << 15) + 200));
    matImg.setTo((1 << 15) + 600, matImg > ((1 << 15) + 600));

    matImg.convertTo(matImg, CV_32FC1);

    double dMax;
    cv::minMaxLoc(matImg, nullptr, &dMax);
    matImg -= (1 << 15) + 200.0f;
    matImg /= 400.0f;
    matOutput.convertTo(matOutput, CV_32FC1);
    matOutput /= dMax;
    
    cv::Mat c = matOutput.clone();
    cv::normalize(c, c, 0, 1., cv::NORM_MINMAX);

    matImg *= 255.f;
    matImg.convertTo(matImg, CV_8UC1);
    cv::threshold(matImg, matImg, 150, 0, cv::THRESH_TOZERO);
    matOutput = c.clone();
    matOutput *= 255;
    matOutput.convertTo(matOutput, CV_8UC1);
    cv::cvtColor(matOutput, matOutput, cv::COLOR_GRAY2BGR);

    cv::Mat matDiff = matImg.clone();
    cv::Mat dist;
    cv::distanceTransform(matDiff, dist, CV_DIST_L2, 3);
    cv::normalize(dist, dist, 0, 1., cv::NORM_MINMAX);
    cv::threshold(dist, dist, .1, 1., CV_THRESH_BINARY);
    
    // Create the CV_8U version of the distance image
    // It is needed for findContours()
    cv::Mat dist_8u;
    dist.convertTo(dist_8u, CV_8U);

    // Find total markers
    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(dist_8u, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
    // Create the marker image for the watershed algorithm
    cv::Mat markers = cv::Mat::zeros(dist.size(), CV_32SC1);
    // Draw the foreground markers
    for (size_t i = 0; i < contours.size(); i++)
    {
      cv::drawContours(markers, contours, static_cast<int>(i), cv::Scalar::all(static_cast<int>(i) + 1), -1);
    }
    cv::cvtColor(matDiff, matDiff, CV_GRAY2BGR);
    // Draw the background marker
    cv::circle(markers, {5, 5}, 3, CV_RGB(255, 255, 255), -1);
    cv::watershed(matDiff, markers);

    std::vector<uint8_t> colors;
    for (size_t i = 1; i <= contours.size(); i++)
    {
      colors.push_back(i);
    }
    // Create the result image
    cv::Mat dst = cv::Mat::zeros(markers.size(), CV_8UC1);
    // Fill labeled objects with random colors
    for (int i = 0; i < markers.rows; i++)
    {
      for (int j = 0; j < markers.cols; j++)
      {
	int index = markers.at<int>(i, j);
	if (index > 0 && index <= static_cast<int>(contours.size()))
        {
          dst.at<uint8_t>(i, j) = colors.at(index - 1);
        }
	else
        {
          dst.at<uint8_t>(i, j) = 0;
        }
      }
    }

    // determine individual objects
    std::vector<featureVec> objects;
    int numOfObjects = contours.size();
    int numRows = dst.rows;
    int numCols = dst.cols;
    uchar* p = nullptr;
    cv::Mat matTmp = matOutput.clone();
    for (int i = 1; i <= numOfObjects; ++i)
    {
      cv::Mat matCopy = dst.clone();
      matCopy.setTo(0, matCopy != i);
      matCopy.setTo(1, matCopy == i);
      cv::Mat points;
      cv::findNonZero(matCopy, points);
      cv::Rect minRect = cv::boundingRect(points);      

      cv::Mat temp = matCopy(minRect);

      // calculate features of the object
      try
      {
	featureVec fV = featureVec({minRect.x, minRect.y}, temp);
	if (fV.getArea() > 20)
        {
	  objects.push_back(fV);
	}
      }
      catch (...) {}
    }

    cv::Mat matOtsuThresh = matOutput.clone();
    cv::cvtColor(matOtsuThresh, matOtsuThresh, cv::COLOR_BGR2GRAY);
    cv::threshold(matOtsuThresh, matOtsuThresh, 0, 255, cv::THRESH_BINARY|cv::THRESH_OTSU);
    std::vector<cv::Point> pts;
    cv::findNonZero(matOtsuThresh, pts);
    cv::RotatedRect minEllipse;
    minEllipse = fitEllipse(pts);
    Contour bodyContour = ellipseToContour(minEllipse);

    // rotated rectangle
    std::vector<Candidate> vCandidates;
    for (const featureVec& f : objects)
    {
      Candidate c(f.getBoundingRect());

      // ratio of longer to shorter edge must be larger than 1.5
      if (c.getQuality() > 1.5)
      {
	cv::Mat matTmpImg = matOutput.clone();
	cv::cvtColor(matTmpImg, matTmpImg, cv::COLOR_BGR2GRAY);
	
	auto rrBest = calculateBestRect(matTmpImg, c.getRotatedRectangle());

	Contour::size_type pos = nearestPointContour(bodyContour, rrBest.center);
	cv::Vec2f vecTangent = tangentVectorAtContourPoint(bodyContour, pos);
	cv::Vec2f vecRR = rrToVec(rrBest);

	// cosine of both vectors must be smaller than 0.7 (preferably 0)
	if (compareVectors(vecTangent, vecRR) < 0.7)
	{
          Candidate c(rrBest);
	  vCandidates.push_back(c);
	}
      }
      
    }

    std::vector<Candidate> connectedCandidates = connectCandidates(vCandidates);
    float fMaxHomogeneity = 0.0f;
    cv::Mat matOutFrameImg = matOutput.clone();
    cv::Mat matHom;
    cv::cvtColor(matOutFrameImg, matHom, cv::COLOR_BGR2GRAY);
    std::vector<Candidate>::iterator itHomogenous = connectedCandidates.begin();
    for (auto it = connectedCandidates.begin(); it != connectedCandidates.end(); ++it)
    {
      float hom = getHomogeneity(matHom, it->getRotatedRectangle());
      if (hom > fMaxHomogeneity)
      {
	hom = fMaxHomogeneity;
	itHomogenous = it;
      }
    }

    cv::Point2f needlePos;
    float needleAngle = NAN;
    {
      cv::Mat img = cv::Mat::zeros(512, 512, CV_8UC1);
      cv::cvtColor(img, img, cv::COLOR_GRAY2BGR);
      if (!connectedCandidates.empty())
      {
	const Candidate& c = *itHomogenous;
	needleAngle = c.getRotatedRectangle().angle;
	cv::Point2f endNeedle = findNeedleTip(matOutput, c.getRotatedRectangle());
	needlePos = endNeedle;
	cv::circle(matOutFrameImg, endNeedle, 3, {255, 0, 255}, -1);

	cv::Point2f startPoint = {std::sin(c.getRotatedRectangle().angle*3.14159f/180.f)*endNeedle.y + endNeedle.x, 0};
	cv::line(matOutFrameImg, startPoint, endNeedle, {0, 127, 255});
      }
      else
      {
	cv::Point maxPos;
	double maxValue;
	cv::Mat matGray;
	cv::cvtColor(matOutFrameImg, matGray, cv::COLOR_BGR2GRAY);
	cv::minMaxLoc(matGray, nullptr, &maxValue, nullptr, &maxPos);
	needlePos = {static_cast<float>(maxPos.x), static_cast<float>(maxPos.y)};
	cv::circle(matOutFrameImg, maxPos, 3, {255, 0, 255}, -1);
      }
    }

/*
    // compare with ground truth
    const GroundTruthSample& gts = csvValues[iFilePos];
    cv::Point2f csvTip = {gts.tipX, gts.tipY};
    cv::Point2f csvEnd = {gts.endX, gts.endY};
    cv::circle(matOutFrameImg, csvTip, 3, {0,0,255}, -1);
    float csvAngle = std::asin((csvEnd.x-csvTip.x)/cv::norm(csvEnd-csvTip))*180.f/3.14159f;
    cv::line(matOutFrameImg, csvTip, csvEnd, {0, 255, 0});

    std::cout << "csv: " << csvTip << ", " << csvAngle << "deg" << std::endl;
    std::cout << "line: " << needlePos << ", " << needleAngle << "deg" << std::endl << std::endl;

    outFile << csvTip << ";" << csvAngle << ";" << needlePos << ";" << needleAngle << ";"
	    << cv::norm(csvTip - needlePos) << ";" << std::abs(csvAngle - needleAngle) << ";"
	    << (cv::norm(csvTip - needlePos) + std::abs(csvAngle - needleAngle)) << "\n";
*/
      
    vw << matOutFrameImg;

    std::cout << "done." << std::endl;
  }
  vw.release();

  return 0;
}
