#include "featureVec.h"
#include <math.h>

featureVec::featureVec(const cv::Point& position, const cv::Mat& segmentation)
{
  // detect contours
  std::vector<std::vector<cv::Point>> contours;
  cv::findContours(segmentation, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

  std::vector<cv::Point> objectContour;
  if (!contours.empty())
  {
    unsigned posMaxArea = 0;
    float maxArea = 0.f;
    for (unsigned i = 0; i < contours.size(); ++i)
    {
      std::vector<cv::Point>& c = contours.at(i);
      float area = cv::contourArea(c);
      if (area > maxArea)
      {
	maxArea = area;
	posMaxArea = i;
      }
    }
    objectContour = contours.at(posMaxArea);
  }
  else
  {
    objectContour.push_back({0, 0});
    objectContour.push_back({segmentation.cols, 0});
    objectContour.push_back({segmentation.cols, segmentation.rows});
    objectContour.push_back({0, segmentation.rows});
  }

  // compute area
  this->area = cv::contourArea(objectContour);

  // compute rotated bounding box
  this->boundingRectangle = cv::minAreaRect(objectContour);
  const cv::Size2f& rectSize = this->boundingRectangle.size;
  cv::Point2f oip[4];
  this->boundingRectangle.points(oip);
  for (cv::Point2f& p : oip) {
    p.x += position.x;
    p.y += position.y;
  }
  this->boundingRectangle = cv::RotatedRect(
    {this->boundingRectangle.center.x + position.x,
	this->boundingRectangle.center.y + position.y},
    this->boundingRectangle.size,
    this->boundingRectangle.angle);

  // compute axis aligned bounding box
  this->aabb = cv::Rect(position, cv::Size(segmentation.cols, segmentation.rows));

}

