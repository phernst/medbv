#pragma once

#include "opencv2/opencv.hpp"
#include <opencv2/imgproc/imgproc.hpp>

class featureVec
{
public:
    featureVec(const cv::Point& position, const cv::Mat& segmentation);

    const cv::RotatedRect& getBoundingRect() const
      { return this->boundingRectangle; }
    const cv::Rect& getAABB() const {return this->aabb;}
    double getArea() const { return this->area; }

private:
    cv::RotatedRect boundingRectangle;
    cv::Rect aabb;
    double area;
};
