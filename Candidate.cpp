#include "Candidate.hpp"

Candidate::Candidate(const cv::RotatedRect& rr)
  : m_rrBoundingRect(rr)
{
  // Quality: ratio of longer to shorter edge
  cv::Point2f points[4];
  rr.points(points);
  m_dQuality = cv::norm(points[0] - points[1])
    /cv::norm(points[1] - points[2]);
  m_dQuality = m_dQuality < 1.0 ? 1/m_dQuality : m_dQuality;

  // Area: of rectangle
  m_dArea = rr.size.height*rr.size.width;
}
