#pragma once

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

class Candidate
{
public:
  explicit Candidate(const cv::RotatedRect& rr);
  
  double getQuality() const { return m_dQuality; }
  double getArea() const { return m_dArea; }
  const cv::RotatedRect& getRotatedRectangle() const { return m_rrBoundingRect; }

private:
  double m_dQuality;
  double m_dArea;
  cv::RotatedRect m_rrBoundingRect;
  
};
